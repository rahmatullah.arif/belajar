<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Papan Catur</title>
</head>

<body>
<table border="1" cellpadding="2" width="300px" height="300px" align="center">
	<tr>
		<td colspan="8" align="center"><b>Papan Catur</b></td>
	</tr>
	<?php
	for ($row=1; $row<=5 ; $row++) { 
		echo "<tr>";
			for ($col=1; $col<=5 ; $col++) { 
				$total = $row+$col;
				if($total%2==0)
				{
					echo "<td style='background:#FFF'>&nbsp;</td>";
				}
				else
				{
					echo "<td style='background:#000'>&nbsp;</td>";	
				}
			}
		echo "</tr>";
	}
	?>
</table>
</body>
</html>

